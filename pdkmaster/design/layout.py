# SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
"""The pdkmaster.design.layout module provides classes to represent layout shapes
in a PDKMaster technology. These classes are designed to only allow to create
layout that conform to the technology definition. In order to detect design
shorts as fast as possible shapes are put on nets.

A LayoutFactory class is provided to generate layouts for a certain technology and
it's primitives.

Internally the klayout API is used to represent the shapes and perform manipulations
on them.
"""
import abc
from pdkmaster.typing import IntFloat, SingleOrMulti
from typing import (
    Any, Iterable, Generator, Sequence, Mapping, Tuple, Dict, Optional, Union,
    Type, cast, overload,
)

from .. import _util, dispatch as dsp
from ..technology import (
    property_ as prp, net as net_, mask as msk, geometry as geo,
    primitive as prm, technology_ as tch,
)
from . import circuit as ckt

__all__ = [
    "MaskShapesSubLayout",
    "SubLayouts",
    "LayoutFactory",
]

_rotations = (
    "no", "90", "180", "270", "mirrorx", "mirrorx&90", "mirrory", "mirrory&90",
)


class NetOverlapError(Exception):
    pass


def _rect(
    left: float, bottom: float, right: float, top: float, *,
    enclosure: Optional[Union[float, Sequence[float], prp.Enclosure]]=None,
) -> geo.Rect:
    if enclosure is not None:
        if isinstance(enclosure, prp.Enclosure):
            enclosure = enclosure.spec
        if isinstance(enclosure, float):
            left -= enclosure
            bottom -= enclosure
            right += enclosure
            top += enclosure
        else:
            left -= enclosure[0]
            bottom -= enclosure[1]
            right += enclosure[0]
            top += enclosure[1]

    return geo.Rect(
        left=left, bottom=bottom, right=right, top=top,
    )


def _via_array(
    left: float, bottom: float, width: float, pitch: float, rows: int, columns: int,
):
    via = geo.Rect.from_size(width=width, height=width)
    xy0 = geo.Point(x=(left + 0.5*width), y=(bottom + 0.5*width))

    if (rows == 1) and (columns == 1):
        return via + xy0
    else:
        return geo.ArrayShape(
            shape=via, offset0=xy0, rows=rows, columns=columns, pitch_x=pitch, pitch_y=pitch,
        )


class _SubLayout(abc.ABC):
    @abc.abstractmethod
    def __init__(self, polygons: geo.MaskShape):
        self.polygons = polygons

    @abc.abstractmethod
    def dup(self):
        raise AssertionError("Internal error")

    @abc.abstractmethod
    def move(
        self, dx, dy, rotation="no", *,
        move_context: Optional[geo.MoveContext]=None,
        rot_context: Optional[geo.RotationContext]=None,
    ):
        if rotation == "no":
            polygons = self.polygons
        else:
            polygons = self.polygons.rotated(
                rotation=geo.Rotation.from_name(rotation),
                context=rot_context,
            )
        self.polygons = polygons.moved(
            dxy=geo.Point(x=dx, y=dy), context=move_context,
        )

    @abc.abstractmethod
    def moved(
        self, dx, dy, rotation="no", *,
        move_context: Optional[geo.MoveContext]=None,
        rot_context: Optional[geo.RotationContext]=None,
    ):
        raise AssertionError("Internal error")

    @property
    @abc.abstractmethod
    def _hier_strs_(self) -> Generator[str, None, None]:
        pass


class MaskShapesSubLayout(_SubLayout):
    """Object representing the sublayout of a net consisting of geometry._Shape
    objects.

    Arguments:
        net: The net of the SubLayout
            `None` value represents no net for the shapes.
        shapes: The maskshapes on the net.
    """
    def __init__(self, *, net: Optional[net_.Net], shapes: geo.MaskShapes):
        self._net = net
        self._shapes = shapes

    @property
    def net(self) -> Optional[net_.Net]:
        return self._net
    @property
    def shapes(self) -> geo.MaskShapes:
        return self._shapes

    def add_shape(self, *, shape: geo.MaskShape):
        self._shapes += shape

    def move(self,
        dx: float, dy: float, rotation: str, *,
        move_context: Optional[geo.MoveContext]=None,
        rot_context: Optional[geo.RotationContext]=None,
    ):
        if rotation == "no":
            shapes = self.shapes
        else:
            shapes = self.shapes.rotated(
                rotation=geo.Rotation.from_name(rotation),
                context=rot_context,
            )
        self._shapes = shapes.moved(
            dxy=geo.Point(x=dx, y=dy), context=move_context,
        )

    def moved(self,
        dx: float, dy: float, rotation: str, *,
        move_context: Optional[geo.MoveContext]=None,
        rot_context: Optional[geo.RotationContext]=None,
    ) -> "MaskShapesSubLayout":
        if rotation == "no":
            shapes = self.shapes
        else:
            r = geo.Rotation.from_name(rotation)
            shapes = self.shapes.rotated(rotation=r, context=rot_context)
        return MaskShapesSubLayout(
            net=self.net,
            shapes=shapes.moved(dxy=geo.Point(x=dx, y=dy), context=move_context),
        )

    def dup(self) -> "MaskShapesSubLayout":
        return MaskShapesSubLayout(
            net=self.net, shapes=geo.MaskShapes(self.shapes),
        )

    @property
    def _hier_strs_(self) -> Generator[str, None, None]:
        yield f"MaskShapesSubLayout net={self.net}"
        for ms in self.shapes:
            yield "  " + str(ms)

    def __hash__(self):
        return hash((self.net, self.shapes))

    def __eq__(self, other: object) -> bool:
        if isinstance(other, MaskShapesSubLayout):
            return (self.net == other.net) and (self.shapes == other.shapes)
        else:
            return False


class _InstanceSubLayout(_SubLayout):
    def __init__(self, inst, *, x, y, layoutname, rotation):
        assert (
            isinstance(inst, ckt._CellInstance)
            and isinstance (x, float) and isinstance(y, float)
            and ((layoutname is None) or isinstance(layoutname, str))
            and (rotation in _rotations)
        ), "Internal error"
        self.inst = inst
        self.x = x
        self.y = y
        self.rotation = rotation
        cell = inst.cell

        if layoutname is None:
            try:
                # Create default layout
                l = cell.layout
            except:
                raise ValueError(
                    f"Cell '{cell.name}' has no default layout and no layoutname"
                    " was specified"
                )
        else:
            if layoutname not in cell.layouts.keys():
                raise ValueError(
                    f"Cell '{cell.name}' has no layout named '{layoutname}'"
                )
            self.layoutname = layoutname
        # layout is a property and will only be looked up the first time it is accessed.
        # This is to support cell with delayed layout generation.
        self._layout = None

    @property
    def layout(self):
        if self._layout is None:
            l = (
                self.inst.cell.layouts[self.layoutname] if hasattr(self, "layoutname")
                else self.inst.cell.layout
            )
            self._layout = l.moved(dx=self.x, dy=self.y, rotation=self.rotation)

        return self._layout

    @property
    def boundary(self) -> geo._Rectangular:
        l = (
            self.inst.cell.layouts[self.layoutname] if hasattr(self, "layoutname")
            else self.inst.cell.layout
        )
        assert l.boundary is not None
        return l.boundary.rotated(
            rotation=geo.Rotation.from_name(self.rotation),
        ) + geo.Point(x=self.x, y=self.y)

    @property
    def polygons(self):
        return self.layout.polygons

    def dup(self):
        return self

    def _rotation(self, rotation):
        x = self.x
        y = self.y
        _xylookup = {
            "no": (x, y),
            "90": (-y, x),
            "180": (-x, -y),
            "270": (y, -x),
            "mirrorx": (-x, y),
            "mirrorx&90": (-y, -x),
            "mirrory": (x, -y),
            "mirrory&90": (y, x),
        }
        _rotlookup = {
            "no": {
                "no": "no",
                "90": "90",
                "180": "180",
                "270": "270",
                "mirrorx": "mirrorx",
                "mirrorx&90": "mirrorx&90",
                "mirrory": "mirrory",
                "mirrory&90": "mirrory&90",
            },
            "90": {
                "no": "90",
                "90": "180",
                "180": "270",
                "270": "no",
                "mirrorx": "mirrory&90",
                "mirrorx&90": "270",
                "mirrory": "mirrorx&90",
                "mirrory&90": "mirrorx",
            },
            "180": {
                "no": "180",
                "90": "270",
                "180": "no",
                "270": "90",
                "mirrorx": "mirrory",
                "mirrorx&90": "mirrory&90",
                "mirrory": "mirrorx",
                "mirrory&90": "mirrorx&90",
            },
            "270": {
                "no": "270",
                "90": "no",
                "180": "90",
                "270": "180",
                "mirrorx": "mirrory&90",
                "mirrorx&90": "mirrorx",
                "mirrory": "mirrorx&90",
                "mirrory&90": "mirrory",
            },
            "mirrorx": {
                "no": "mirrorx",
                "90": "mirrorx&90",
                "180": "mirrory",
                "270": "mirrory&90",
                "mirrorx": "no",
                "mirrorx&90": "90",
                "mirrory": "180",
                "mirrory&90": "270",
            },
            "mirrorx&90": {
                "no": "mirrorx&90",
                "90": "mirrory",
                "180": "mirrory&90",
                "270": "mirrorx",
                "mirrorx": "270",
                "mirrorx&90": "no",
                "mirrory": "90",
                "mirrory&90": "180",
            },
            "mirrory": {
                "no": "mirrory",
                "90": "mirrory&90",
                "180": "mirrorx",
                "270": "mirrorx&90",
                "mirrorx": "180",
                "mirrorx&90": "270",
                "mirrory": "no",
                "mirrory&90": "90",
            },
            "mirrory&90": {
                "no": "mirrory&90",
                "90": "mirrorx",
                "180": "mirrorx&90",
                "270": "mirrory",
                "mirrorx": "90",
                "mirrorx&90": "180",
                "mirrory": "270",
                "mirrory&90": "no",
            },
        }

        return (*_xylookup[rotation], _rotlookup[self.rotation][rotation])

    def move(
        self, dx, dy, rotation="no", *,
        move_context: Optional[geo.MoveContext]=None,
        rot_context: Optional[geo.RotationContext]=None,
    ):
        x, y, rot2 = self._rotation(rotation)
        self.x += x + dx
        self.y += y + dy
        self.rotation = rot2
        self._layout = None

    def moved(
        self, dx, dy, rotation="no", *,
        move_context: Optional[geo.MoveContext]=None,
        rot_context: Optional[geo.RotationContext]=None,
    ):
        x, y, rot2 = self._rotation(rotation)
        return _InstanceSubLayout(
            self.inst, x=(x + dx), y=(y + dy),
            layoutname=(self.layoutname if hasattr(self, "layoutname") else None),
            rotation=rot2
        )

    @property
    def _hier_strs_(self) -> Generator[str, None, None]:
        yield f"_InstanceSubLayout inst={self.inst}, x={self.x}, y={self.y}, rot={self.rotation}"
        for s in self.layout._hier_strs_:
            yield "  " + s


class SubLayouts(_util.TypedList[_SubLayout]):
    @property
    def _elem_type_(self):
        return _SubLayout

    def __init__(self, iterable: SingleOrMulti[_SubLayout].T=tuple()):
        if isinstance(iterable, _SubLayout):
            super().__init__((iterable,))
        else:
            super().__init__(iterable)

            nets = tuple(sl.net for sl in self.__iter_type__(MaskShapesSubLayout))
            if len(nets) != len(set(nets)):
                raise ValueError("Multiple `MaskShapesSubLayout` for same net")

    def dup(self) -> "SubLayouts":
        return SubLayouts(l.dup() for l in self)

    def __iadd__(self, other_: SingleOrMulti[_SubLayout].T) -> "SubLayouts":
        other: Iterable[_SubLayout]
        if isinstance(other_, _SubLayout):
            other = (other_,)
        else:
            other = tuple(other_)

        # Now try to add to other sublayouts
        def add2other(other_sublayout):
            if isinstance(other_sublayout, MaskShapesSubLayout):
                for sublayout in self.__iter_type__(MaskShapesSubLayout):
                    if sublayout.net == other_sublayout.net:
                        for shape in other_sublayout.shapes:
                            sublayout.add_shape(shape=shape)
                        return True
                else:
                    return False
            elif not isinstance(other_sublayout, _InstanceSubLayout):
                raise AssertionError("Internal error")
        other = tuple(filter(lambda sl: not add2other(sl), other))

        if other:
            # Append remaining sublayouts
            self.extend(sl.dup() for sl in other)
        return self

    def __add__(self, other: SingleOrMulti[_SubLayout].T) -> "SubLayouts":
        ret = self.dup()
        ret += other
        return ret


class _Layout:
    def __init__(self, *,
        fab: "LayoutFactory",
        sublayouts: SubLayouts, boundary: Optional[geo._Rectangular]=None,
    ):
        assert (
            isinstance(fab, LayoutFactory)
            and isinstance(sublayouts, SubLayouts)
        ), "Internal error"
        self.fab = fab
        self.sublayouts = sublayouts
        self.boundary = boundary

    @property
    def polygons(self) -> Generator[geo.MaskShape, None, None]:
        for sublayout in self.sublayouts:
            if isinstance(sublayout, MaskShapesSubLayout):
                yield from sublayout.shapes
            else:
                assert isinstance(sublayout.polygons, (Generator))
                yield from sublayout.polygons

    def _net_sublayouts(self, *, net: net_.Net, depth: Optional[int]) -> Generator[
        MaskShapesSubLayout, None, None,
    ]:
        for sl in self.sublayouts:
            if isinstance(sl, _InstanceSubLayout):
                assert isinstance(net, ckt._CircuitNet)
                if depth != 0:
                    for port in net.childports:
                        if (
                            isinstance(port, ckt._InstanceNet)
                            and (port.inst == sl.inst)
                        ):
                            yield from sl.layout._net_sublayouts(
                                net=port.net,
                                depth=(None if depth is None else (depth - 1)),
                            )
            elif isinstance(sl, MaskShapesSubLayout):
                if net == sl.net:
                    yield sl
            else:
                raise AssertionError("Internal error")

    def net_polygons(self, net: net_.Net, *, depth: Optional[int]=None) -> Generator[
        geo.MaskShape, None, None
    ]:
        if not isinstance(net, net_.Net):
            raise TypeError("net has to be of type 'Net'")
        for sl in self._net_sublayouts(net=net, depth=depth):
            if isinstance(sl, MaskShapesSubLayout):
                yield from sl.shapes
            else:
                yield from sl.polygons

    def filter_polygons(self, *,
        net: Optional[net_.Net]=None, mask: Optional[msk._Mask]=None,
        split: bool=False, depth: Optional[int]=None,
    ) -> Generator[geo.MaskShape, None, None]:
        if net is None:
            sls = self.sublayouts
        else:
            sls = self._net_sublayouts(net=net, depth=depth)
        for sl in sls:
            assert isinstance(sl, MaskShapesSubLayout)
            if mask is None:
                shapes = sl.shapes
            else:
                shapes = filter(lambda sh: sh.mask == mask, sl.shapes)
            if not split:
                yield from shapes
            else:
                for shape in shapes:
                    for shape2 in shape.shape.pointsshapes:
                        yield geo.MaskShape(mask=shape.mask, shape=shape2)

    def dup(self) -> "_Layout":
        return _Layout(
            fab=self.fab,
            sublayouts=SubLayouts(sl.dup() for sl in self.sublayouts),
            boundary=self.boundary,
        )

    def bounds(self, *,
        mask: Optional[msk._Mask]=None, net: Optional[net_.Net]=None,
        depth: Optional[int]=None,
    ) -> geo.Rect:
        if net is None:
            if depth is not None:
                raise TypeError(
                    f"depth has to 'None' if net is 'None'"
                )
            polygons = self.polygons
        else:
            polygons = self.net_polygons(net, depth=depth)
        mps = polygons if mask is None else filter(
            lambda mp: mp.mask == mask, polygons,
        )
        boundslist = tuple(mp.bounds for mp in mps)
        return geo.Rect(
            left=min(bds.left for bds in boundslist),
            bottom=min(bds.bottom for bds in boundslist),
            right=max(bds.right for bds in boundslist),
            top=max(bds.top for bds in boundslist),
        )

    def __iadd__(self, other):
        if self.sublayouts._frozen_:
            raise ValueError("Can't add sublayouts to a frozen 'Layout' object")
        if not isinstance(other, (_Layout, _SubLayout, SubLayouts)):
            raise TypeError(
                "Can only add '_Layout', '_SubLayout' or 'SubLayouts' object to"
                " a '_Layout' object"
            )

        self.sublayouts += (
            other.sublayouts if isinstance(other, _Layout) else other
        )

        return self

    def add_primitive(self, *,
        prim: prm._Primitive, x: float=0.0, y: float=0.0, rotation: str="no",
        **prim_params,
    ) -> "_Layout":
        if not (prim in self.fab.tech.primitives):
            raise ValueError(
                f"prim '{prim.name}' is not a primitive of technology"
                f" '{self.fab.tech.name}'"
            )
        if rotation not in _rotations:
            raise ValueError(
                f"rotation '{rotation}' is not one of {_rotations}"
            )

        primlayout = self.fab.new_primitivelayout(prim, **prim_params)
        primlayout.move(dx=x, dy=y, rotation=rotation)
        self += primlayout
        return primlayout

    def add_wire(self, *,
        net: net_.Net, wire: prm._Conductor, shape: Optional[geo._Shape]=None,
        **wire_params,
    ) -> "_Layout":
        if (shape is None) or isinstance(shape, geo.Rect):
            if shape is not None:
                # TODO: Add support in _PrimitiveLayouter for shape argument,
                # e.g. non-rectangular shapes
                c = shape.center
                wire_params.update({
                    "x": c.x, "y": c.y,
                    "width": shape.width, "height": shape.height,
                })
            return self.add_primitive(
                portnets={"conn": net}, prim=wire, **wire_params,
            )
        else:
            pin = wire_params.pop("pin", None)
            if len(wire_params) != 0:
                raise TypeError(
                    f"params {wire_params.keys()} not supported for shape not of type 'Rect'",
                )
            l = self.fab.new_layout()
            self.add_shape(net=net, prim=wire, shape=shape)
            l.add_shape(net=net, prim=wire, shape=shape)
            if pin is not None:
                self.add_shape(net=net, prim=pin, shape=shape)
                l.add_shape(net=net, prim=pin, shape=shape)
            return l

    def add_maskshape(self, *, net: Optional[net_.Net]=None, maskshape: geo.MaskShape):
        """Add a geometry MaskShape to a _Layout
        """
        for sl in self.sublayouts.__iter_type__(MaskShapesSubLayout):
            if sl.net == net:
                sl.add_shape(shape=maskshape)
                break
        else:
            self.sublayouts += MaskShapesSubLayout(
                net=net, shapes=geo.MaskShapes(maskshape),
            )

    def add_shape(self, *,
        prim: prm._DesignMaskPrimitive, net: Optional[net_.Net]=None, shape: geo._Shape,
    ):
        """Add a geometry _Shape to a _Layout
        """
        self.add_maskshape(
            net=net,
            maskshape=geo.MaskShape(mask=cast(msk.DesignMask, prim.mask), shape=shape),
        )

    def move(self, dx, dy, rotation="no"):
        move_context = geo.MoveContext()
        rot_context = geo.RotationContext()
        for sl in self.sublayouts:
            sl.move(
                dx, dy, rotation, move_context=move_context, rot_context=rot_context,
            )

    def moved(self, dx, dy, rotation="no"):
        move_context = geo.MoveContext()
        rot_context = geo.RotationContext()

        if self.boundary is None:
            bound = None
        else:
            bound = self.boundary
            if rotation != "no":
                bound = bound.rotated(
                    rotation=geo.Rotation.from_name(rotation), context=rot_context,
                )
            bound = bound.moved(dxy=geo.Point(x=dx, y=dy), context=move_context)
        return _Layout(
            fab=self.fab, sublayouts=SubLayouts(
                sl.moved(dx, dy, rotation, move_context=move_context, rot_context=rot_context)
                for sl in self.sublayouts
            ),
            boundary=bound,
        )

    def freeze(self):
        self.sublayouts._freeze_()

    @property
    def _hier_str_(self) -> str:
        return "\n  ".join(("layout:", *(s for s in self._hier_strs_)))

    @property
    def _hier_strs_(self) -> Generator[str, None, None]:
        for sl in self.sublayouts:
            yield from sl._hier_strs_

    def __eq__(self, other: Any):
        if isinstance(other, _Layout):
            return self.sublayouts == other.sublayouts
        else:
            return False


class _PrimitiveLayouter(dsp.PrimitiveDispatcher):
    def __init__(self, fab: "LayoutFactory"):
        self.fab = fab

    def __call__(self, prim: prm._Primitive, *args, **kwargs) -> _Layout:
        return super().__call__(prim, *args, **kwargs)

    @property
    def tech(self):
        return self.fab.tech

    # Dispatcher implementation
    def _Primitive(self, prim: prm._Primitive, **params):
        raise NotImplementedError(
            f"Don't know how to generate minimal layout for primitive '{prim.name}'\n"
            f"of type '{prim.__class__.__name__}'"
        )

    def Marker(self, prim: prm.Marker, **params) -> _Layout:
        if ("width" in params) and ("height" in params):
            return self._WidthSpacePrimitive(cast(prm._WidthSpacePrimitive, prim), **params)
        else:
            return super().Marker(prim, **params)

    def _WidthSpacePrimitive(self,
        prim: prm._WidthSpacePrimitive, **widthspace_params,
    ) -> _Layout:
        if len(prim.ports) != 0:
            raise NotImplementedError(
                f"Don't know how to generate minimal layout for primitive '{prim.name}'\n"
                f"of type '{prim.__class__.__name__}'"
            )
        width = widthspace_params["width"]
        height = widthspace_params["height"]
        r = geo.Rect.from_size(width=width, height=height)

        l = self.fab.new_layout()
        assert isinstance(prim, prm._DesignMaskPrimitive)
        l.add_shape(prim=prim, shape=r)
        return l

    def _WidthSpaceConductor(self,
        prim: prm._WidthSpaceConductor, **conductor_params,
    ) -> _Layout:
        assert (
            (len(prim.ports) == 1) and (prim.ports[0].name == "conn")
        ), "Internal error"
        width = conductor_params["width"]
        height = conductor_params["height"]
        r = geo.Rect.from_size(width=width, height=height)

        try:
            portnets = conductor_params["portnets"]
        except KeyError:
            net = prim.ports.conn
        else:
            net = portnets["conn"]

        layout = self.fab.new_layout()
        layout.add_shape(prim=prim, net=net, shape=r)
        pin = conductor_params.get("pin", None)
        if pin is not None:
            layout.add_shape(prim=pin, net=net, shape=r)

        return layout

    def WaferWire(self, prim: prm.WaferWire, **waferwire_params) -> _Layout:
        width = waferwire_params["width"]
        height = waferwire_params["height"]

        implant = waferwire_params.pop("implant")
        implant_enclosure = waferwire_params.pop("implant_enclosure")
        assert implant_enclosure is not None

        well = waferwire_params.pop("well", None)
        well_enclosure = waferwire_params.pop("well_enclosure", None)

        oxide = waferwire_params.pop("oxide", None)
        oxide_enclosure = waferwire_params.pop("oxide_enclosure", None)

        layout = self._WidthSpaceConductor(prim, **waferwire_params)
        layout.add_shape(prim=implant, shape=_rect(
            -0.5*width, -0.5*height, 0.5*width, 0.5*height,
            enclosure=implant_enclosure,
        ))
        if well is not None:
            try:
                well_net = waferwire_params["well_net"]
            except KeyError:
                raise TypeError(
                    f"No well_net given for WaferWire '{prim.name}' in well '{well.name}'"
                )
            layout.add_shape(prim=well, net=well_net, shape=_rect(
                -0.5*width, -0.5*height, 0.5*width, 0.5*height,
                enclosure=well_enclosure,
            ))
        if oxide is not None:
            layout.add_shape(prim=oxide, shape=_rect(
                -0.5*width, -0.5*height, 0.5*width, 0.5*height,
                enclosure=oxide_enclosure,
            ))
        return layout

    def Via(self, prim: prm.Via, **via_params) -> _Layout:
        tech = self.tech

        try:
            portnets = via_params["portnets"]
        except KeyError:
            net = prim.ports["conn"]
        else:
            if set(portnets.keys()) != {"conn"}:
                raise ValueError(f"Via '{prim.name}' needs one net for the 'conn' port")
            net = portnets["conn"]

        bottom = via_params["bottom"]
        bottom_enc = via_params["bottom_enclosure"]
        if (bottom_enc is None) or isinstance(bottom_enc, str):
            idx = prim.bottom.index(bottom)
            enc = prim.min_bottom_enclosure[idx]
            if bottom_enc is None:
                bottom_enc = enc
            elif bottom_enc == "wide":
                bottom_enc = enc.wide()
            else:
                assert bottom_enc == "tall"
                bottom_enc = enc.tall()
        assert isinstance(bottom_enc, prp.Enclosure)
        if isinstance(bottom_enc.spec, float):
            bottom_enc_x = bottom_enc_y = bottom_enc.spec
        else:
            bottom_enc_x = bottom_enc.spec[0]
            bottom_enc_y = bottom_enc.spec[1]

        top = via_params["top"]
        top_enc = via_params["top_enclosure"]
        if (top_enc is None) or isinstance(top_enc, str):
            idx = prim.top.index(top)
            enc = prim.min_top_enclosure[idx]
            if top_enc is None:
                top_enc = enc
            elif top_enc == "wide":
                top_enc = enc.wide()
            else:
                assert top_enc == "tall"
                top_enc = enc.tall()
        assert isinstance(top_enc, prp.Enclosure)
        if isinstance(top_enc.spec, float):
            top_enc_x = top_enc_y = top_enc.spec
        else:
            top_enc_x = top_enc.spec[0]
            top_enc_y = top_enc.spec[1]

        width = prim.width
        space = via_params["space"]
        pitch = width + space

        rows = via_params["rows"]
        bottom_height = via_params["bottom_height"]
        top_height = via_params["top_height"]
        if rows is None:
            if bottom_height is None:
                assert top_height is not None
                rows = int(self.tech.on_grid(top_height - 2*top_enc_y - width)//pitch + 1)
                via_height = rows*pitch - space
                bottom_height = tech.on_grid(
                    via_height + 2*bottom_enc_y, mult=2, rounding="ceiling",
                )
            else:
                rows = int(self.tech.on_grid(bottom_height - 2*bottom_enc_y - width)//pitch + 1)
                if top_height is not None:
                    rows = min(
                        rows,
                        int(self.tech.on_grid(top_height - 2*top_enc_y - width)//pitch + 1),
                    )
                via_height = rows*pitch - space
                if top_height is None:
                    top_height = tech.on_grid(
                        via_height + 2*top_enc_y, mult=2, rounding="ceiling",
                    )
        else:
            assert (bottom_height is None) and (top_height is None)
            via_height = rows*pitch - space
            bottom_height = tech.on_grid(
                via_height + 2*bottom_enc_y, mult=2, rounding="ceiling",
            )
            top_height = tech.on_grid(
                via_height + 2*top_enc_y, mult=2, rounding="ceiling",
            )

        columns = via_params["columns"]
        bottom_width = via_params["bottom_width"]
        top_width = via_params["top_width"]
        if columns is None:
            if bottom_width is None:
                assert top_width is not None
                columns = int(self.tech.on_grid(top_width - 2*top_enc_x - width)//pitch + 1)
                via_width = columns*pitch - space
                bottom_width = tech.on_grid(
                    via_width + 2*bottom_enc_x, mult=2, rounding="ceiling",
                )
            else:
                columns = int(self.tech.on_grid(bottom_width - 2*bottom_enc_x - width)//pitch + 1)
                if top_width is not None:
                    columns = min(
                        columns,
                        int(self.tech.on_grid(top_width - 2*top_enc_x - width)//pitch + 1)
                    )
                via_width = columns*pitch - space
                if top_width is None:
                    top_width = tech.on_grid(
                        via_width + 2*top_enc_x, mult=2, rounding="ceiling",
                    )
        else:
            assert (bottom_width is None) and (top_width is None)
            via_width = columns*pitch - space
            bottom_width = tech.on_grid(
                via_width + 2*bottom_enc_x, mult=2, rounding="ceiling",
            )
            top_width = tech.on_grid(
                via_width + 2*top_enc_x, mult=2, rounding="ceiling",
            )

        bottom_left = tech.on_grid(-0.5*bottom_width, rounding="floor")
        bottom_bottom = tech.on_grid(-0.5*bottom_height, rounding="floor")
        bottom_right = bottom_left + bottom_width
        bottom_top = bottom_bottom + bottom_height
        bottom_rect = geo.Rect(
            left=bottom_left, bottom=bottom_bottom,
            right=bottom_right, top=bottom_top,
        )

        top_left = tech.on_grid(-0.5*top_width, rounding="floor")
        top_bottom = tech.on_grid(-0.5*top_height, rounding="floor")
        top_right = top_left + top_width
        top_top = top_bottom + top_height
        top_rect = geo.Rect(
            left=top_left, bottom=top_bottom,
            right=top_right, top=top_top,
        )

        via_bottom = tech.on_grid(-0.5*via_height)
        via_left = tech.on_grid(-0.5*via_width)

        layout = cast(_Layout, self.fab.new_layout())

        layout.add_shape(prim=bottom, net=net, shape=bottom_rect)
        layout.add_shape(prim=prim, net=net, shape=_via_array(
            via_left, via_bottom, width, pitch, rows, columns,
        ))
        layout.add_shape(prim=top, net=net, shape=top_rect)
        try:
            impl = via_params["bottom_implant"]
        except KeyError:
            impl = None
        else:
            if impl is not None:
                enc = cast(prp.Enclosure, via_params["bottom_implant_enclosure"])
                assert enc is not None, "Internal error"
                layout.add_shape(prim=impl, shape=geo.Rect.from_rect(
                    rect=bottom_rect, bias=enc,
                ))
        try:
            oxide = via_params["bottom_oxide"]
        except KeyError:
            oxide = None
        else:
            if oxide is not None:
                assert (
                    isinstance(bottom, prm.WaferWire) and (bottom.oxide is not None)
                    and (bottom.min_oxide_enclosure is not None)
                )
                enc = cast(prp.Enclosure, via_params["bottom_oxide_enclosure"])
                if enc is None:
                    idx = bottom.oxide.index(oxide)
                    enc = bottom.min_oxide_enclosure[idx]
                assert (enc is not None), "Unknown enclosure"
                layout.add_shape(prim=oxide, shape=geo.Rect.from_rect(
                    rect=bottom_rect, bias=enc,
                ))
        try:
            well = via_params["bottom_well"]
        except KeyError:
            well = None
        else:
            if well is not None:
                well_net = via_params.get("well_net", None)
                enc = via_params["bottom_well_enclosure"]
                assert enc is not None, "Internal error"
                if (impl is not None) and (impl.type_ == well.type_):
                    if well_net is not None:
                        if well_net != net:
                            raise ValueError(
                                f"Net '{well_net}' for well '{well.name}' of WaferWire"
                                f" {bottom.name} is different from net '{net.name}''\n"
                                f"\tbut implant '{impl.name}' is same type as the well"
                            )
                    else:
                        well_net = net
                elif well_net is None:
                    raise TypeError(
                        f"No well_net specified for WaferWire '{bottom.name}' in"
                        f" well '{well.name}'"
                    )
                layout.add_shape(prim=well, net=well_net, shape=geo.Rect.from_rect(
                    rect=bottom_rect, bias=enc,
                ))

        return layout

    def Resistor(self, prim: prm.Resistor, **resistor_params) -> _Layout:
        try:
            portnets = resistor_params["portnets"]
        except KeyError:
            port1 = prim.ports.port1
            port2 = prim.ports.port2
        else:
            if set(portnets.keys()) != {"port1", "port2"}:
                raise ValueError(
                    f"Resistor '{prim.name}' needs two port nets ('port1', 'port2')"
                )
            port1 = portnets["port1"]
            port2 = portnets["port2"]
        if prim.contact is None:
            raise NotImplementedError("Resistor layout without contact layer")

        res_width = resistor_params["width"]
        res_height = resistor_params["height"]

        wire = prim.wire

        cont = prim.contact
        cont_space = prim.min_contact_space
        assert cont_space is not None
        try:
            wire_idx = cont.bottom.index(wire)
        except ValueError:
            try:
                wire_idx = cont.top.index(wire)
            except ValueError:
                raise AssertionError("Internal error")
            else:
                cont_enc = cont.min_top_enclosure[wire_idx]
                cont_args = {"top": wire, "x": 0.0, "top_width": res_width}
        else:
            cont_enc = cont.min_bottom_enclosure[wire_idx]
            cont_args = {"bottom": wire, "x": 0.0, "bottom_width": res_width}
        cont_y1 = -0.5*res_height - cont_space - 0.5*cont.width
        cont_y2 = -cont_y1

        wire_ext = cont_space + cont.width + cont_enc.min()

        layout = self.fab.new_layout()

        # Draw indicator layers
        for idx, ind in enumerate(prim.indicator):
            ext = prim.min_indicator_extension[idx]
            layout += self(ind, width=(res_width + 2*ext), height=res_height)

        # Draw wire layer
        mp = geo.MultiPartShape(
            fullshape=geo.Rect.from_size(
                width=res_width, height=(res_height + 2*wire_ext),
            ),
            parts = (
                geo.Rect.from_floats(values=(
                    -0.5*res_width, -0.5*res_height - wire_ext,
                    0.5*res_width, -0.5*res_height,
                )),
                geo.Rect.from_floats(values=(
                    -0.5*res_width, -0.5*res_height,
                    0.5*res_width, 0.5*res_height,
                )),
                geo.Rect.from_floats(values=(
                    -0.5*res_width, 0.5*res_height,
                    0.5*res_width, 0.5*res_height + wire_ext,
                )),
            )
        )
        layout.add_shape(prim=wire, net=port1, shape=mp.parts[0])
        layout.add_shape(prim=wire, shape=mp.parts[1])
        layout.add_shape(prim=wire, net=port2, shape=mp.parts[2])

        # Draw contacts
        # Hack to make sure the bottom wire does not overlap with the resistor part
        # TODO: Should be fixed in MultiPartShape handling
        # layout.add_wire(net=port1, wire=cont, y=cont_y1, **cont_args)
        # layout.add_wire(net=port2, wire=cont, y=cont_y2, **cont_args)
        x = cont_args.pop("x")
        _l_cont = self.fab.new_primitivelayout(
            prim=cont, portnets={"conn": port1}, **cont_args
        )
        _l_cont.move(dx=x, dy=cont_y1)
        for sl in _l_cont.sublayouts:
            if isinstance(sl, MaskShapesSubLayout):
                for msl in sl.shapes:
                    if msl.mask == wire.mask:
                        assert isinstance(msl.shape, geo.Rect)
                        msl._shape = geo.Rect.from_rect(
                            rect=msl.shape, top=(-0.5*res_height - self.tech.grid)
                        )
        layout += _l_cont
        _l_cont = self.fab.new_primitivelayout(
            prim=cont, portnets={"conn": port2}, **cont_args
        )
        _l_cont.move(dx=x, dy=cont_y2)
        for sl in _l_cont.sublayouts:
            if isinstance(sl, MaskShapesSubLayout):
                for msl in sl.shapes:
                    if msl.mask == wire.mask:
                        assert isinstance(msl.shape, geo.Rect)
                        msl._shape = geo.Rect.from_rect(
                            rect=msl.shape, bottom=(0.5*res_height + self.tech.grid)
                        )
        layout += _l_cont

        if prim.implant is not None:
            impl = prim.implant
            try:
                enc = prim.min_implant_enclosure.max() # type: ignore
            except AttributeError:
                assert isinstance(wire, prm.WaferWire), "Internal error"
                idx = wire.implant.index(impl)
                enc = wire.min_implant_enclosure[idx].max()
            impl_width = res_width + 2*enc
            impl_height = res_height + 2*wire_ext + 2*enc
            layout.add_shape(prim=impl, shape=geo.Rect.from_size(width=impl_width, height=impl_height))

        return layout

    def Diode(self, prim: prm.Diode, **diode_params) -> _Layout:
        try:
            portnets = diode_params.pop("portnets")
        except KeyError:
            an = prim.ports.anode
            cath = prim.ports.cathode
        else:
            if set(portnets.keys()) != {"anode", "cathode"}:
                raise ValueError(
                    f"Diode '{prim.name}' needs two port nets ('anode', 'cathode')"
                )
            an = portnets["anode"]
            cath = portnets["cathode"]

        if prim.min_implant_enclosure is not None:
            raise NotImplementedError(
                "Diode layout generation with min_implant_enclosure specified"
            )
        wirenet_args = {
            "implant": prim.implant,
            "net": an if prim.implant.type_ == "p" else cath,
        }
        if prim.well is not None:
            wirenet_args.update({
                "well": prim.well,
                "well_net": cath if prim.implant.type_ == "p" else an,
            })

        layout = self.fab.new_layout()
        layout.add_wire(wire=prim.wire, **wirenet_args, **diode_params)
        wireact_bounds = layout.bounds(mask=prim.wire.mask)
        act_width = wireact_bounds.right - wireact_bounds.left
        act_height = wireact_bounds.top - wireact_bounds.bottom

        for i, ind in enumerate(prim.indicator):
            enc = prim.min_indicator_enclosure[i].max()
            layout += self(ind, width=(act_width + 2*enc), height=(act_height + 2*enc))

        return layout

    def MOSFET(self, prim: prm.MOSFET, **mos_params) -> _Layout:
        l = mos_params["l"]
        w = mos_params["w"]
        impl_enc = mos_params["activeimplant_enclosure"]
        gate_encs = mos_params["gateimplant_enclosures"]
        sdw = mos_params["sd_width"]

        try:
            portnets = cast(Mapping[str, net_.Net], mos_params["portnets"])
        except KeyError:
            portnets = prim.ports

        gate_left = -0.5*l
        gate_right = 0.5*l
        gate_top = 0.5*w
        gate_bottom = -0.5*w

        layout = self.fab.new_layout()

        active = prim.gate.active
        active_width = l + 2*sdw
        active_left = -0.5*active_width
        active_right = 0.5*active_width
        active_bottom = gate_bottom
        active_top = gate_top

        mps = geo.MultiPartShape(
            fullshape=geo.Rect.from_size(width=active_width, height=w),
            parts=(
                geo.Rect(
                    left=active_left, bottom=active_bottom,
                    right=gate_left, top=active_top,
                ),
                geo.Rect(
                    left=gate_left, bottom =active_bottom,
                    right=gate_right, top=active_top,
                ),
                geo.Rect(
                    left=gate_right, bottom =active_bottom,
                    right=active_right, top=active_top,
                ),
            )
        )
        layout.add_shape(prim=active, net=portnets["sourcedrain1"], shape=mps.parts[0])
        layout.add_shape(prim=active, net=portnets["bulk"], shape=mps.parts[1])
        layout.add_shape(prim=active, net=portnets["sourcedrain2"], shape=mps.parts[2])

        for impl in prim.implant:
            if impl in active.implant:
                layout.add_shape(prim=impl, shape=_rect(
                    active_left, active_bottom, active_right, active_top,
                    enclosure=impl_enc
                ))

        poly = prim.gate.poly
        ext = prim.computed.min_polyactive_extension
        poly_left = gate_left
        poly_bottom = gate_bottom - ext
        poly_right = gate_right
        poly_top = gate_top + ext
        layout.add_shape(prim=poly, net=portnets["gate"], shape=geo.Rect(
            left=poly_left, bottom=poly_bottom, right=poly_right, top=poly_top,
        ))

        if prim.well is not None:
            enc = active.min_well_enclosure[active.well.index(prim.well)]
            layout.add_shape(prim=prim.well, net=portnets["bulk"], shape=_rect(
                active_left, active_bottom, active_right, active_top, enclosure=enc,
            ))

        oxide = prim.gate.oxide
        if oxide is not None:
            assert (active.oxide is not None) and (active.min_oxide_enclosure is not None)
            enc = getattr(
                prim.gate, "min_gateoxide_enclosure", prp.Enclosure(self.tech.grid),
            )
            layout.add_shape(prim=oxide, shape=_rect(
                gate_left, gate_bottom, gate_right, gate_top, enclosure=enc,
            ))
            idx = active.oxide.index(oxide)
            enc = active.min_oxide_enclosure[idx]
            if enc is not None:
                layout.add_shape(prim=oxide, shape=_rect(
                    active_left, active_bottom, active_right, active_top,
                    enclosure=enc,
                ))
        if prim.gate.inside is not None:
            # TODO: Check is there is an enclosure rule from oxide around active
            # and apply the if so.
            for i, inside in enumerate(prim.gate.inside):
                enc = (
                    prim.gate.min_gateinside_enclosure[i]
                    if prim.gate.min_gateinside_enclosure is not None
                    else prp.Enclosure(self.tech.grid)
                )
                layout.add_shape(prim=inside, shape=_rect(
                    gate_left, gate_bottom, gate_right, gate_top, enclosure=enc,
                ))
        for i, impl in enumerate(prim.implant):
            enc = gate_encs[i]
            layout.add_shape(prim=impl, shape=_rect(
                gate_left, gate_bottom, gate_right, gate_top, enclosure=enc,
            ))

        return layout


class MOSFETInstSpec:
    """Class that provided the spec for the string of transistors generation.

    Used by `_CircuitLayouter.transistors_layout()`

    Arguments:
        inst: the transistor instance to generate layout for in the string.
            A ValueError will be raised in the it is not a MOSFET instance.
            The inst parameters like l, w, etc with determine the layout of the transistor.
        contact_left, contact_right: whether to place contacts left or right from the
            transistor. This value needs to be the same between two neighbours.
    """
    def __init__(self, *,
        inst: ckt._PrimitiveInstance,
        contact_left: Optional[prm.Via], contact_right: Optional[prm.Via],
    ):
        self._inst = inst
        self._contact_left = contact_left
        self._contact_right = contact_right

        if not isinstance(inst.prim, prm.MOSFET):
            raise ValueError(f"inst is not a MOSFET instance")
        mosfet = inst.prim

        if contact_left is not None:
            if len(contact_left.top) != 1:
                raise NotImplementedError(
                    f"Multiple top layers for Via '{contact_left.name}'",
                )
        if contact_right is not None:
            if len(contact_right.top) != 1:
                raise NotImplementedError(
                    f"Multiple top layers for Via '{contact_right.name}'",
                )

        impls = tuple(filter(
            lambda impl: isinstance(impl, prm.Implant) and impl.type_ != "adjust",
            mosfet.implant,
        ))
        if len(impls) != 1:
            raise NotImplementedError(
                f"Multiple implant for MOSFET '{inst.prim.name}'",
            )
        self._implant = impls[0]

    @property
    def inst(self) -> ckt._PrimitiveInstance:
        return self._inst
    @property
    def contact_left(self) -> Optional[prm.Via]:
        return self._contact_left
    @property
    def contact_right(self) -> Optional[prm.Via]:
        return self._contact_right


class _CircuitLayouter:
    def __init__(self, *,
        fab: "LayoutFactory", circuit: ckt._Circuit, boundary: Optional[geo._Rectangular]
    ):
        self.fab = fab
        self.circuit = circuit

        self.layout = l = fab.new_layout()
        l.boundary = boundary

    @property
    def tech(self) -> tch.Technology:
        return self.circuit.fab.tech

    def inst_layout(self, inst: ckt._Instance, *,
        layoutname: Optional[str]=None, rotation: str="no",
    ) -> _Layout:
        if rotation not in _rotations:
            raise ValueError(
                f"rotation '{rotation}' is not one of {_rotations}"
            )

        if isinstance(inst, ckt._PrimitiveInstance):
            notfound = []
            portnets = {}
            for port in inst.ports:
                try:
                    net = self.circuit.net_lookup(port=port)
                except ValueError:
                    notfound.append(port.name)
                else:
                    portnets[port.name] = net
            if len(notfound) > 0:
                raise ValueError(
                    f"Unconnected port(s) {notfound}"
                    f" for inst '{inst.name}' of primitive '{inst.prim.name}'"
                )
            l = self.fab.new_primitivelayout(
                prim=inst.prim, portnets=portnets,
                **inst.params,
            )
            if rotation != "no":
                l.move(dx=0.0, dy=0.0, rotation=rotation)
            return l
        elif isinstance(inst, ckt._CellInstance):
            # TODO: propoer checking of nets for instance
            layout = None
            if layoutname is None:
                try:
                    circuitname = cast(Any, inst).circuitname
                    layout = inst.cell.layouts[circuitname]
                except:
                    layout = inst.cell.layout
                else:
                    layoutname = circuitname
            else:
                if not isinstance(layoutname, str):
                    raise TypeError(
                        "layoutname has to be 'None' or a string, not of type"
                        f" '{type(layoutname)}'"
                    )
                layout = inst.cell.layouts[layoutname]

            return _Layout(
                fab=self.fab,
                sublayouts=SubLayouts(_InstanceSubLayout(
                    inst, x=0.0, y=0.0, layoutname=layoutname, rotation=rotation,
                )),
                boundary=layout.boundary,
            )
        else:
            raise AssertionError("Internal error")

    def wire_layout(self, *,
        net: ckt._CircuitNet, wire: prm._Primitive, **wire_params,
    ) -> _Layout:
        if net not in self.circuit.nets:
            raise ValueError(
                f"net '{net.name}' is not a net of circuit '{self.circuit.name}'"
            )
        if not (
            hasattr(wire, "ports")
            and (len(wire.ports) == 1)
            and (wire.ports[0].name == "conn")
        ):
            raise TypeError(
                f"Wire '{wire.name}' does not have exactly one port named 'conn'"
            )

        return self.fab.new_primitivelayout(
            wire, portnets={"conn": net}, **wire_params,
        )

    def transistors_layout(self, *,
        trans_specs: Iterable[MOSFETInstSpec]
    ) -> _Layout:
        """This method allows to generate a string of transistors.

        Arguments:
            trans_specs: the list of the spec for the transistors to generate. A
                `MOSFETInstSpec` object needs to be provided for each transistor of the
                striog. For more information refer to the `MOSFETInstSpec` reference.
                Some compatibility checks are done on the specification between the right
                specifation of a spec and the left specification of the next. Currently it
                is checked that whether to generata a contact is the same and if the active
                layer between the two transistors is the same.

        Results:
            The string of transistor according to the provided specs from left to right.
        """
        specs = tuple(trans_specs)

        # Check consistency of the specification
        for i, spec in enumerate(specs[:-1]):
            next_spec = specs[i+1]
            mosfet = cast(prm.MOSFET, spec.inst.prim)
            next_mosfet = cast(prm.MOSFET, next_spec.inst.prim)

            if spec.contact_right != next_spec.contact_left:
                raise ValueError(
                    f"Contact specification mismatch between transistor spec {i} and {i+1}",
                )
            if mosfet.gate.active != next_mosfet.gate.active:
                raise ValueError(
                    f"Active specification mismatch between transistor spec {i} and {i+1}",
                )

        # Create the layout

        layout = self.fab.new_layout()
        x = 0.0
        for i, spec in enumerate(specs):
            prev_spec = specs[i - 1] if (i > 0) else None
            next_spec = specs[i + 1] if (i < (len(specs) - 1)) else None
            mosfet = cast(prm.MOSFET, spec.inst.prim)

            # First generate, so the port net checks are run now.
            l_trans = self.inst_layout(spec.inst)

            # Draw left sd
            if spec.contact_left is not None:
                w = spec.inst.params["w"]
                if prev_spec is not None:
                    w = min(w, prev_spec.inst.params["w"])
                if mosfet.well is None:
                    well_args = {}
                else:
                    well_args = {
                        "bottom_well": mosfet.well,
                        "well_net": spec.inst.ports["bulk"],
                    }
                if (i > 0):
                    # Add oxide layer on first contact
                    oxide_args = {}
                else:
                    oxide_args = {"bottom_oxide": mosfet.gate.oxide}
                l = self.wire_layout(
                    wire=spec.contact_left,
                    net=self.circuit.net_lookup(port=spec.inst.ports["sourcedrain1"]),
                    bottom_height=w, bottom=mosfet.gate.active,
                    bottom_implant=spec._implant, **oxide_args, **well_args,
                ).moved(dx=x, dy=0.0)
                layout += l
                x += (
                    0.5*spec.contact_left.width + spec.inst.params["contactgate_space"]
                    + 0.5*spec.inst.params["l"]
                )
            else:
                gate_space = spec.inst.params["gate_space"]
                if prev_spec is not None:
                    gate_space = max(gate_space, prev_spec.inst.params["gate_space"])
                x += 0.5*gate_space + 0.5*spec.inst.params["l"]

            # Remember trans position
            l_trans.move(dx=x, dy=0)
            layout += l_trans

            if spec.contact_right is not None:
                x += (
                    0.5*spec.inst.params["l"] + spec.inst.params["contactgate_space"]
                    + 0.5*spec.contact_right.width
                )
            else:
                gate_space = spec.inst.params["gate_space"]
                if next_spec is not None:
                    gate_space = max(gate_space, next_spec.inst.params["gate_space"])
                x += 0.5*spec.inst.params["l"] + 0.5*gate_space

        # Draw last contact if needed
        spec = specs[-1]
        if spec.contact_right is not None:
            mosfet = cast(prm.MOSFET, spec.inst.prim)
            if mosfet.well is None:
                well_args = {}
            else:
                well_args = {
                    "bottom_well": mosfet.well,
                    "well_net": spec.inst.ports["bulk"],
                }
            l = self.wire_layout(
                wire=spec.contact_right,
                net=self.circuit.net_lookup(port=spec.inst.ports["sourcedrain2"]),
                bottom_height=spec.inst.params["w"], bottom=mosfet.gate.active,
                bottom_implant=spec._implant, bottom_oxide=mosfet.gate.oxide, **well_args,
            ).moved(dx=x, dy=0.0)
            layout += l

        return layout

    @overload
    def place(self, object_: ckt._Instance, *,
        x: IntFloat, y: IntFloat, layoutname: Optional[str]=None, rotation: str="no",
    ) -> _Layout:
        ...
    @overload
    def place(self, object_: _Layout, *,
        x: IntFloat, y: IntFloat, layoutname: None=None, rotation: str="no",
    ) -> _Layout:
        ...
    def place(self, object_, *,
        x: IntFloat, y: IntFloat, layoutname: Optional[str]=None, rotation: str="no",
    ) -> _Layout:
        if rotation not in _rotations:
            raise ValueError(
                f"rotation '{rotation}' is not one of {_rotations}"
            )

        if isinstance(object_, ckt._Instance):
            inst = object_
            if inst not in self.circuit.instances:
                raise ValueError(
                    f"inst '{inst.name}' is not part of circuit '{self.circuit.name}'"
                )
            x = _util.i2f(x)
            y = _util.i2f(y)
            if not all((isinstance(x, float), isinstance(y, float))):
                raise TypeError("x and y have to be floats")

            if isinstance(inst, ckt._PrimitiveInstance):
                def _portnets():
                    for net in self.circuit.nets:
                        for port in net.childports:
                            if (inst == port.inst):
                                yield (port.name, net)
                portnets = dict(_portnets())
                portnames = set(inst.ports.keys())
                portnetnames = set(portnets.keys())
                if not (portnames == portnetnames):
                    raise ValueError(
                        f"Unconnected port(s) {portnames - portnetnames}"
                        f" for inst '{inst.name}' of primitive '{inst.prim.name}'"
                    )
                return self.layout.add_primitive(
                    prim=inst.prim, x=x, y=y, rotation=rotation, portnets=portnets,
                    **inst.params,
                )
            elif isinstance(inst, ckt._CellInstance):
                # TODO: propoer checking of nets for instance
                if (
                    (layoutname is None)
                    and inst.circuitname is not None
                    and (inst.circuitname in inst.cell.layouts.keys())
                ):
                    layoutname = inst.circuitname
                sl = _InstanceSubLayout(
                    inst, x=x, y=y, layoutname=layoutname, rotation=rotation,
                )
                self.layout += sl

                return _Layout(
                    fab=self.fab, sublayouts=SubLayouts(sl), boundary=sl.boundary,
                )
            else:
                raise RuntimeError("Internal error: unsupported instance type")
        elif isinstance(object_, _Layout):
            layout = object_.moved(x, y, rotation=rotation)
            self.layout += layout
            return layout
        else:
            raise AssertionError("Internal error")

    def add_shape(self, *,
        prim: prm._DesignMaskPrimitive, net: Optional[net_.Net], shape: geo._Shape,
    ):
        """Add a geometry shape to a _Layout

        This is lower level tool. One is adviced to use higher level add_...() methods
        if possible.
        """
        if (net is not None) and (net not in self.circuit.nets):
            raise ValueError(
                f"net '{net.name}' is not a net of circuit '{self.circuit.name}'"
            )

        self.layout.add_shape(prim=prim, net=net, shape=shape)

    def add_wire(self, *, net, wire, **wire_params) -> _Layout:
        if net not in self.circuit.nets:
            raise ValueError(
                f"net '{net.name}' is not a net of circuit '{self.circuit.name}'"
            )
        return self.layout.add_wire(
            net=net, wire=wire, **wire_params,
        )

    def add_portless(self, *,
        prim: prm._DesignMaskPrimitive, shape: Optional[geo._Shape]=None, **prim_params,
    ):
        if len(prim.ports) > 0:
            raise ValueError(
                f"prim '{prim.name}' should not have any port"
            )

        if shape is None:
            return self.layout.add_primitive(prim=prim, **prim_params)
        else:
            if len(prim_params) != 0:
                raise ValueError(
                    f"Parameters '{tuple(prim_params.keys())}' not supported for shape not 'None'",
                )
            self.add_shape(prim=prim, net=None, shape=shape)


class LayoutFactory:
    def __init__(self, tech):
        if not isinstance(tech, tch.Technology):
            raise TypeError("tech has to be of type Technology")
        self.tech = tech
        self.gen_primlayout = _PrimitiveLayouter(self)

    def new_layout(self, *,
        sublayouts: Optional[Union[_SubLayout, SubLayouts]]=None,
        boundary: Optional[geo._Rectangular]=None,
        cls: Type[_Layout]=_Layout
    ):
        if sublayouts is None:
            sublayouts = SubLayouts()
        if isinstance(sublayouts, _SubLayout):
            sublayouts = SubLayouts(sublayouts)

        return cls(fab=self, sublayouts=sublayouts, boundary=boundary)

    def new_primitivelayout(self, prim, **prim_params) -> _Layout:
        prim_params = prim.cast_params(prim_params)
        return self.gen_primlayout(prim, **prim_params)

    def new_circuitlayouter(self, *,
        circuit:ckt._Circuit, boundary: Optional[geo._Rectangular],
    ) -> _CircuitLayouter:
        return _CircuitLayouter(fab=self, circuit=circuit, boundary=boundary)

    def spec4bound(self, *, bound_spec, via=None):
        spec_out = {}
        if via is None:
            if isinstance(bound_spec, dict):
                specs = ("left", "bottom", "right", "top")
                if not all(spec in specs for spec in bound_spec.keys()):
                    raise ValueError(f"Bound spec for non-Via are {specs}")

                if "left" in bound_spec:
                    if "right" not in bound_spec:
                        raise ValueError(
                            "expecting both 'left' and 'right' spec or none of them"
                        )
                    left = bound_spec["left"]
                    right = bound_spec["right"]
                    spec_out.update({"x": (left + right)/2.0, "width": right - left})
                elif "right" in bound_spec:
                    raise ValueError(
                        "expecting both 'left' and 'right' or none of them"
                    )

                if "bottom" in bound_spec:
                    if "top" not in bound_spec:
                        raise ValueError(
                            "expecting both 'bottom' and 'top' spec or none of them"
                        )
                    bottom = bound_spec["bottom"]
                    top = bound_spec["top"]
                    spec_out.update({"y": (bottom + top)/2.0, "height": top - bottom})
                elif "top" in bound_spec:
                    raise ValueError(
                        "expecting both 'bottom' and 'top' spec or none of them"
                    )
            else:
                if not isinstance(bound_spec, geo.Rect):
                    bound_spec = geo.Rect.from_floats(
                        values=cast(
                            Tuple[float, float, float, float],
                            tuple(bound_spec),
                        ),
                    )
                spec_out.update({
                    "x": (bound_spec.left + bound_spec.right)/2.0,
                    "y": (bound_spec.bottom + bound_spec.top)/2.0,
                    "width": bound_spec.right - bound_spec.left,
                    "height": bound_spec.top - bound_spec.bottom,
                })
        else:
            if not isinstance(via, prm.Via):
                raise TypeError("via has to be 'None' or of type 'Via'")
            specs = (
                "space",
                "bottom_layer", "bottom_enclosure",
                "top_layer", "top_enclosure",
                "bottom_left", "bottom_bottom", "bottom_right", "bottom_top",
                "top_left", "top_bottom", "top_right", "top_top",
            )
            if not all(spec in specs for spec in bound_spec.keys()):
                raise ValueError(f"Bound specs for a Via are:\n  {specs}")

            try:
                space = bound_spec["space"]
            except KeyError:
                space = via.min_space
            else:
                spec_out["space"] = space

            try:
                bottom_layer = bound_spec["bottom_layer"]
            except KeyError:
                bottom_layer = via.bottom[0]
                idx = 0
            else:
                idx = via.bottom.index(bottom_layer)
            try:
                bottom_enc = bound_spec["bottom_enclosure"]
            except KeyError:
                bottom_enc = via.min_bottom_enclosure[idx]
            else:
                spec_out["bottom_enclosure"] = bottom_enc
            bottom_enc = bottom_enc.spec
            if isinstance(bottom_enc, float):
                bottom_enc = (bottom_enc, bottom_enc)

            try:
                top_layer = bound_spec["top_layer"]
            except KeyError:
                top_layer = via.top[0] if len(via.top) == 1 else None
                idx = 0
            else:
                idx = via.top.index(top_layer)
            try:
                top_enc = bound_spec["top_enclosure"]
            except KeyError:
                top_enc = via.min_top_enclosure[idx]
            else:
                spec_out["top_enclosure"] = top_enc

            top_enc = top_enc.spec
            if isinstance(top_enc, float):
                top_enc = (top_enc, top_enc)

            via_left = via_bottom = via_right = via_top = None
            if "bottom_left" in bound_spec:
                if "top_left" in bound_spec:
                    via_left = max((
                        bound_spec["bottom_left"] + bottom_enc[0],
                        bound_spec["top_left"] + top_enc[0],
                    ))
                else:
                    via_left = bound_spec["bottom_left"] + bottom_enc[0]
            elif "top_left" in bound_spec:
                via_left = bound_spec["top_left"] + top_enc[0]

            if "bottom_bottom" in bound_spec:
                if "top_bottom" in bound_spec:
                    via_bottom = max((
                        bound_spec["bottom_bottom"] + bottom_enc[1],
                        bound_spec["top_bottom"] + top_enc[1],
                    ))
                else:
                    via_bottom = bound_spec["bottom_bottom"] + bottom_enc[1]
            elif "top_bottom" in bound_spec:
                via_bottom = bound_spec["top_bottom"] + top_enc[1]

            if "bottom_right" in bound_spec:
                if "top_right" in bound_spec:
                    via_right = min((
                        bound_spec["bottom_right"] - bottom_enc[0],
                        bound_spec["top_right"] - top_enc[0],
                    ))
                else:
                    via_right = bound_spec["bottom_right"] - bottom_enc[0]
            elif "top_right" in bound_spec:
                via_right = bound_spec["top_right"] - top_enc[0]

            if "bottom_top" in bound_spec:
                if "top_top" in bound_spec:
                    via_top = min((
                        bound_spec["bottom_top"] - bottom_enc[1],
                        bound_spec["top_top"] - top_enc[1],
                    ))
                else:
                    via_top = bound_spec["bottom_top"] - bottom_enc[1]
            elif "top_top" in bound_spec:
                via_top = bound_spec["top_top"] - top_enc[1]

            if (via_left is not None) and (via_right is not None):
                width = via_right - via_left
                columns = int((width - via.width)/(via.width + space)) + 1
                if columns < 1:
                    raise ValueError("Not enough width for fitting one column")
                spec_out.update({
                    "x": self.tech.on_grid((via_left + via_right)/2.0),
                    "columns": columns,
                })
            elif (via_left is not None) or (via_right is not None):
                raise ValueError("left/right spec mismatch")

            if (via_bottom is not None) and (via_top is not None):
                height = via_top - via_bottom
                rows = int((height - via.width)/(via.width + space)) + 1
                if rows < 1:
                    raise ValueError("Not enough height for fitting one row")
                spec_out.update({
                    "y": self.tech.on_grid((via_bottom + via_top)/2.0),
                    "rows": rows,
                })
            elif (via_bottom is not None) or (via_top is not None):
                raise ValueError("bottom/top spec mismatch")

        if not spec_out:
            raise ValueError("No specs found")

        return spec_out
