# SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
# type: ignore
import unittest

from pdkmaster.technology import geometry as _geo
from pdkmaster.design import layout as _lay

from ..dummy import dummy_tech
prims = dummy_tech.primitives

class LayoutTest(unittest.TestCase):
    def test_sublayouts(self):
        mask = prims.metal.mask

        r1 = _geo.Rect.from_floats(values=(0.0, 0.0, 1.0, 1.0))
        r2 = _geo.Rect.from_floats(values=(1.0, 0.0, 2.0, 1.0))

        ms1 = _geo.MaskShapes(_geo.MaskShape(mask=mask, shape=r1))
        ms2 = _geo.MaskShapes(_geo.MaskShape(mask=mask, shape=r2))

        sl1 = _lay.MaskShapesSubLayout(net=None, shapes=ms1)
        sl2 = _lay.MaskShapesSubLayout(net=None, shapes=ms2)

        with self.assertRaises(ValueError):
            _lay.SubLayouts((sl1, sl2))
